import React from "react";
import { FaGamepad, FaUsers, FaComments } from "react-icons/fa";
import "./home.css";
import Logo from "../assets/logo-no-background.png";
import image1 from "../assets/Church of the Machine God_ Photo.jpg"; // Importa l'immagine 1
import image2 from "../assets/Church of the Machine God_ Photo.jpg"; // Importa l'immagine 2
function Home() {
  const features = [
    {
      title: "Caratteristica 1",
      description: "Descrizione della caratteristica 1",
      image: image1, // Utilizza l'immagine 1
    },
    {
      title: "Caratteristica 2",
      description: "Descrizione della caratteristica 2",
      image: image2, // Utilizza l'immagine 2
    },
    // Aggiungi altre caratteristiche con le rispettive immagini...
  ];
  return (
    <div className="home-container">
      <div className="welcome-message">
        <img width={"400"} src={Logo} alt="" srcset="" />
      </div>
      <div className="features-container">
        {features.map((feature, index) => (
          <div key={index} className="feature">
            <img src={feature.image} alt={feature.title} className="icon" />
            <h2 className="feature-title">{feature.title}</h2>
            <p className="feature-description">{feature.description}</p>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Home;
