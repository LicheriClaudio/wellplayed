import React, { useState } from "react";
import { BiHome, BiUser, BiGame, BiMessage } from "react-icons/bi";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import "./sidebar.css"; // Crea il file Sidebar.css per gli stili

const Sidebar = () => {
  //TODO implement close and open now is always
  const [isOpen, setIsOpen] = useState(true);

  const toggleSidebar = () => {
    setIsOpen((prevState) => !prevState);
  };

  const sidebarItems = [
    { id: 1, label: "Home", icon: BiHome },
    { id: 2, label: "Friends", icon: BiUser },
    { id: 3, label: "Games", icon: BiGame },
    { id: 4, label: "Messages", icon: BiMessage },
  ];
  return (
    <div className={`sidebar ${isOpen ? "open" : "closed"}`}>
      <div className="logo-container">
        {/* <img src={Logo} alt="Descrizione dell'immagine" /> */}
        <h1 className="logo">WellPlayed</h1>
        {!isOpen && (
          <div className="toggle-btn" onClick={toggleSidebar}>
            {/* Freccia che punta a sinistra quando la sidebar è chiusa */}
            <span className="arrow left"></span>
          </div>
        )}
      </div>
      <ul className="sidebar-menu">
        {sidebarItems.map((item) => (
          <li key={item.id}>
            <OverlayTrigger
              placement="right"
              overlay={<Tooltip>{item.label}</Tooltip>}
            >
              <a href="#" className="sidebar-link">
                <item.icon className="sidebar-icon" />
              </a>
            </OverlayTrigger>
          </li>
        ))}
      </ul>
      {!isOpen && (
        <div className="toggle-btn" onClick={toggleSidebar}>
          {/* Freccia che punta a destra quando la sidebar è chiusa */}
          <span className="arrow right"></span>
        </div>
      )}
    </div>
  );
};

export default Sidebar;
