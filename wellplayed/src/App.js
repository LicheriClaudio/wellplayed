import Home from "./home/home";
import logo from "./logo.svg";
import "./App.css";
import Sidebar from "./sidebar/sidebar";
function App() {
  return (
    <>
      <div className="container">
        <Sidebar />
      </div>
      <div className="container">
        <Home />
      </div>
    </>
  );
}

export default App;
